import { Template } from 'meteor/templating';
import { Hugs } from '../imports/init.js';
import './body.html';

Template.hugs.events({
    'click button.hug'(event, instance) {
        Hugs.upsert({_id: 1}, {$inc: {hugs: 1}})
    },
    'click button.overhug'(event, instance) {
        Hugs.upsert({_id: 2}, {$inc: {hugs: 1}})
    }
});
var getHugs = function(id) {
    var hug = Hugs.findOne({_id: id});
    if (typeof hug != 'undefined') {
        return hug.hugs;
    } else {
        return 0;
    }
};
var getQueryArg = function(arg) {
    var query = window.location.search.slice(1);
    var pairs = query.split('&');
    var split;
    for (var i = 0; i < pairs.length; i++) {
        console.log(pairs[i]);
        split = pairs[i].split('=');
        var key = split[0];
        var value;
        if (split.length > 1) {
            value = split[1];
        } else {
            value = null;
        }
        if (key == arg) {
            console.log(value);
            return decodeURIComponent(value);
        }
    }
    return null;
};
Template.hugs.helpers({
    hugs() {
        return getHugs(1);
    },
    overhugs() {
        return getHugs(2);
    },
    lashes() {
        return getHugs(2) * 20;
    },
    bgcolor() {
        return getQueryArg('bgcolor') == null ? 'transparent' : getQueryArg('bgcolor');
    },
    fontcolor() {
        return getQueryArg('fontcolor') == null ? 'black' : getQueryArg('fontcolor');
    }
});
